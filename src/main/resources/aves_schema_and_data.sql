--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.6
-- Dumped by pg_dump version 10.4 (Ubuntu 10.4-0ubuntu0.18.04)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: aves; Type: SCHEMA; Schema: -; Owner: avesadmin
--

CREATE SCHEMA aves;


ALTER SCHEMA aves OWNER TO avesadmin;

--
-- Name: update_modified_column(); Type: FUNCTION; Schema: aves; Owner: avesadmin
--

CREATE FUNCTION aves.update_modified_column() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    NEW.modified_ts = current_timestamp;
    RETURN NEW; 
END;
$$;


ALTER FUNCTION aves.update_modified_column() OWNER TO avesadmin;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: company; Type: TABLE; Schema: aves; Owner: avesadmin
--

CREATE TABLE aves.company (
    id integer NOT NULL,
    name text NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    created_by text,
    created_ts timestamp without time zone DEFAULT now(),
    modified_by text,
    modified_ts timestamp without time zone DEFAULT now()
);


ALTER TABLE aves.company OWNER TO avesadmin;

--
-- Name: company_id_seq; Type: SEQUENCE; Schema: aves; Owner: avesadmin
--

CREATE SEQUENCE aves.company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aves.company_id_seq OWNER TO avesadmin;

--
-- Name: company_id_seq; Type: SEQUENCE OWNED BY; Schema: aves; Owner: avesadmin
--

ALTER SEQUENCE aves.company_id_seq OWNED BY aves.company.id;


--
-- Name: location; Type: TABLE; Schema: aves; Owner: avesadmin
--

CREATE TABLE aves.location (
    id integer NOT NULL,
    name text NOT NULL,
    address_line_1 text,
    address_line_2 text,
    city text,
    state text,
    zipcode integer,
    is_deleted boolean DEFAULT false NOT NULL,
    created_by text,
    created_ts timestamp without time zone DEFAULT now(),
    modified_by text,
    modified_ts timestamp without time zone DEFAULT now()
);


ALTER TABLE aves.location OWNER TO avesadmin;

--
-- Name: location_id_seq; Type: SEQUENCE; Schema: aves; Owner: avesadmin
--

CREATE SEQUENCE aves.location_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aves.location_id_seq OWNER TO avesadmin;

--
-- Name: location_id_seq; Type: SEQUENCE OWNED BY; Schema: aves; Owner: avesadmin
--

ALTER SEQUENCE aves.location_id_seq OWNED BY aves.location.id;


--
-- Name: login; Type: TABLE; Schema: aves; Owner: avesadmin
--

CREATE TABLE aves.login (
    user_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    username text NOT NULL,
    password text NOT NULL,
    last_login_ts timestamp without time zone,
    status_id integer,
    is_deleted boolean DEFAULT false NOT NULL,
    deleted_by_user_id uuid,
    created_by text,
    created_ts timestamp without time zone DEFAULT now(),
    modified_by text,
    modified_ts timestamp without time zone DEFAULT now()
);


ALTER TABLE aves.login OWNER TO avesadmin;

--
-- Name: manager_team; Type: TABLE; Schema: aves; Owner: avesadmin
--

CREATE TABLE aves.manager_team (
    id integer NOT NULL,
    user_id uuid NOT NULL,
    team_id integer NOT NULL,
    created_by text,
    created_ts timestamp without time zone DEFAULT now(),
    modified_by text,
    modified_ts timestamp without time zone DEFAULT now()
);


ALTER TABLE aves.manager_team OWNER TO avesadmin;

--
-- Name: manager_team_id_seq; Type: SEQUENCE; Schema: aves; Owner: avesadmin
--

CREATE SEQUENCE aves.manager_team_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aves.manager_team_id_seq OWNER TO avesadmin;

--
-- Name: manager_team_id_seq; Type: SEQUENCE OWNED BY; Schema: aves; Owner: avesadmin
--

ALTER SEQUENCE aves.manager_team_id_seq OWNED BY aves.manager_team.id;


--
-- Name: position; Type: TABLE; Schema: aves; Owner: avesadmin
--

CREATE TABLE aves."position" (
    id integer NOT NULL,
    name text NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    created_by text,
    created_ts timestamp without time zone DEFAULT now(),
    modified_by text,
    modified_ts timestamp without time zone DEFAULT now()
);


ALTER TABLE aves."position" OWNER TO avesadmin;

--
-- Name: position_id_seq; Type: SEQUENCE; Schema: aves; Owner: avesadmin
--

CREATE SEQUENCE aves.position_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aves.position_id_seq OWNER TO avesadmin;

--
-- Name: position_id_seq; Type: SEQUENCE OWNED BY; Schema: aves; Owner: avesadmin
--

ALTER SEQUENCE aves.position_id_seq OWNED BY aves."position".id;


--
-- Name: role; Type: TABLE; Schema: aves; Owner: avesadmin
--

CREATE TABLE aves.role (
    id integer NOT NULL,
    name text NOT NULL,
    description text,
    is_deleted boolean DEFAULT false NOT NULL,
    created_by text,
    created_ts timestamp without time zone DEFAULT now(),
    modified_by text,
    modified_ts timestamp without time zone DEFAULT now()
);


ALTER TABLE aves.role OWNER TO avesadmin;

--
-- Name: role_id_seq; Type: SEQUENCE; Schema: aves; Owner: avesadmin
--

CREATE SEQUENCE aves.role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aves.role_id_seq OWNER TO avesadmin;

--
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: aves; Owner: avesadmin
--

ALTER SEQUENCE aves.role_id_seq OWNED BY aves.role.id;


--
-- Name: status_lookup; Type: TABLE; Schema: aves; Owner: avesadmin
--

CREATE TABLE aves.status_lookup (
    id integer NOT NULL,
    status text NOT NULL,
    description text,
    is_deleted boolean DEFAULT false NOT NULL,
    created_by text,
    created_ts timestamp without time zone DEFAULT now(),
    modified_by text,
    modified_ts timestamp without time zone DEFAULT now()
);


ALTER TABLE aves.status_lookup OWNER TO avesadmin;

--
-- Name: status_lookup_id_seq; Type: SEQUENCE; Schema: aves; Owner: avesadmin
--

CREATE SEQUENCE aves.status_lookup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aves.status_lookup_id_seq OWNER TO avesadmin;

--
-- Name: status_lookup_id_seq; Type: SEQUENCE OWNED BY; Schema: aves; Owner: avesadmin
--

ALTER SEQUENCE aves.status_lookup_id_seq OWNED BY aves.status_lookup.id;


--
-- Name: team; Type: TABLE; Schema: aves; Owner: avesadmin
--

CREATE TABLE aves.team (
    id integer NOT NULL,
    name text NOT NULL,
    parent_team_id integer,
    is_deleted boolean DEFAULT false NOT NULL,
    created_by text,
    created_ts timestamp without time zone DEFAULT now(),
    modified_by text,
    modified_ts timestamp without time zone DEFAULT now()
);


ALTER TABLE aves.team OWNER TO avesadmin;

--
-- Name: team_id_seq; Type: SEQUENCE; Schema: aves; Owner: avesadmin
--

CREATE SEQUENCE aves.team_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aves.team_id_seq OWNER TO avesadmin;

--
-- Name: team_id_seq; Type: SEQUENCE OWNED BY; Schema: aves; Owner: avesadmin
--

ALTER SEQUENCE aves.team_id_seq OWNED BY aves.team.id;


--
-- Name: user; Type: TABLE; Schema: aves; Owner: avesadmin
--

CREATE TABLE aves."user" (
    id uuid DEFAULT public.uuid_generate_v4(),
    first_name text,
    last_name text,
    nick_name text,
    email text,
    phone text,
    subteam_id integer,
    location_id integer,
    company_id integer,
    position_id integer,
    is_free_agent boolean DEFAULT false NOT NULL,
    available_date date,
    is_deleted boolean DEFAULT false NOT NULL,
    deleted_by_user_id uuid,
    created_by text,
    created_ts timestamp without time zone DEFAULT now(),
    modified_by text,
    modified_ts timestamp without time zone DEFAULT now()
);


ALTER TABLE aves."user" OWNER TO avesadmin;

--
-- Name: user_activation; Type: TABLE; Schema: aves; Owner: avesadmin
--

CREATE TABLE aves.user_activation (
    id integer NOT NULL,
    user_id uuid NOT NULL,
    verification_hash character varying(256),
    created_by text,
    created_ts timestamp without time zone DEFAULT now(),
    modified_by text,
    modified_ts timestamp without time zone DEFAULT now()
);


ALTER TABLE aves.user_activation OWNER TO avesadmin;

--
-- Name: user_activation_id_seq; Type: SEQUENCE; Schema: aves; Owner: avesadmin
--

CREATE SEQUENCE aves.user_activation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aves.user_activation_id_seq OWNER TO avesadmin;

--
-- Name: user_activation_id_seq; Type: SEQUENCE OWNED BY; Schema: aves; Owner: avesadmin
--

ALTER SEQUENCE aves.user_activation_id_seq OWNED BY aves.user_activation.id;


--
-- Name: user_role; Type: TABLE; Schema: aves; Owner: avesadmin
--

CREATE TABLE aves.user_role (
    id integer NOT NULL,
    user_id uuid NOT NULL,
    role_id integer NOT NULL,
    created_by text,
    created_ts timestamp without time zone DEFAULT now(),
    modified_by text,
    modified_ts timestamp without time zone DEFAULT now()
);


ALTER TABLE aves.user_role OWNER TO avesadmin;

--
-- Name: user_role_id_seq; Type: SEQUENCE; Schema: aves; Owner: avesadmin
--

CREATE SEQUENCE aves.user_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aves.user_role_id_seq OWNER TO avesadmin;

--
-- Name: user_role_id_seq; Type: SEQUENCE OWNED BY; Schema: aves; Owner: avesadmin
--

ALTER SEQUENCE aves.user_role_id_seq OWNED BY aves.user_role.id;


--
-- Name: company id; Type: DEFAULT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.company ALTER COLUMN id SET DEFAULT nextval('aves.company_id_seq'::regclass);


--
-- Name: location id; Type: DEFAULT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.location ALTER COLUMN id SET DEFAULT nextval('aves.location_id_seq'::regclass);


--
-- Name: manager_team id; Type: DEFAULT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.manager_team ALTER COLUMN id SET DEFAULT nextval('aves.manager_team_id_seq'::regclass);


--
-- Name: position id; Type: DEFAULT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves."position" ALTER COLUMN id SET DEFAULT nextval('aves.position_id_seq'::regclass);


--
-- Name: role id; Type: DEFAULT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.role ALTER COLUMN id SET DEFAULT nextval('aves.role_id_seq'::regclass);


--
-- Name: status_lookup id; Type: DEFAULT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.status_lookup ALTER COLUMN id SET DEFAULT nextval('aves.status_lookup_id_seq'::regclass);


--
-- Name: team id; Type: DEFAULT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.team ALTER COLUMN id SET DEFAULT nextval('aves.team_id_seq'::regclass);


--
-- Name: user_activation id; Type: DEFAULT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.user_activation ALTER COLUMN id SET DEFAULT nextval('aves.user_activation_id_seq'::regclass);


--
-- Name: user_role id; Type: DEFAULT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.user_role ALTER COLUMN id SET DEFAULT nextval('aves.user_role_id_seq'::regclass);


--
-- Data for Name: company; Type: TABLE DATA; Schema: aves; Owner: avesadmin
--

COPY aves.company (id, name, is_deleted, created_by, created_ts, modified_by, modified_ts) FROM stdin;
1	Boyer Inc	f	load	2018-07-07 16:04:30.512765	load	2018-07-07 16:04:30.512765
2	Grant-Batz	f	load	2018-07-07 16:04:30.512765	load	2018-07-07 16:04:30.512765
3	Block, Rolfson and Kemmer	f	load	2018-07-07 16:04:30.512765	load	2018-07-07 16:04:30.512765
4	Cormier Inc	f	load	2018-07-07 16:04:30.512765	load	2018-07-07 16:04:30.512765
5	Kemmer-Trantow	f	load	2018-07-07 16:04:30.512765	load	2018-07-07 16:04:30.512765
6	Heathcote and Sons	f	load	2018-07-07 16:04:30.512765	load	2018-07-07 16:04:30.512765
7	Towne, Marvin and Windler	f	load	2018-07-07 16:04:30.512765	load	2018-07-07 16:04:30.512765
8	MacGyver Group	f	load	2018-07-07 16:04:30.512765	load	2018-07-07 16:04:30.512765
9	Gaylord, Runolfsdottir and Kemmer	f	load	2018-07-07 16:04:30.512765	load	2018-07-07 16:04:30.512765
10	Beahan-Barton	f	load	2018-07-07 16:04:30.512765	load	2018-07-07 16:04:30.512765
\.


--
-- Data for Name: location; Type: TABLE DATA; Schema: aves; Owner: avesadmin
--

COPY aves.location (id, name, address_line_1, address_line_2, city, state, zipcode, is_deleted, created_by, created_ts, modified_by, modified_ts) FROM stdin;
1	Reston	1851 Alexander Bell Dr. #300	\N	Reston	Virginia	20191	f	leef	2018-07-15 15:40:55.308	leef	2018-07-15 15:43:35.315257
\.

--
-- Data for Name: login; Type: TABLE DATA; Schema: aves; Owner: avesadmin
--

COPY aves.login (user_id, username, password, last_login_ts, status_id, is_deleted, deleted_by_user_id, created_by, created_ts, modified_by, modified_ts) FROM stdin;
0003fd6a-1cac-4a00-a4e5-8ac6997da302	corena	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
b4f3ac6a-5531-42ef-a4b7-e648ef02788b	theodora	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
2b726fc3-8d3f-41eb-bb11-2eeb384b36c9	ely	$2a$10$GS0usbIXXc2JbXNHw2KhyuGLuzi3Z00qOFNxaQxsPPvl8vbeHK1bW	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
41196387-0ab2-4c29-90b4-a5111525826d	buddy	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
7b2e2138-459d-469c-ad85-35d4a292244f	thor	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
ee965630-16eb-4cfd-bb60-ab1bff3c4744	nealson	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
6f5a5ad3-4353-4469-97f7-32d27668b0c7	zea	$2a$10$GS0usbIXXc2JbXNHw2KhyuGLuzi3Z00qOFNxaQxsPPvl8vbeHK1bW	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
6de53471-6947-4024-942f-fe1c7abd6d7e	samson	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
248e550c-a896-4736-9a99-e031896d93ac	corbett	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
4fbbc4db-4c7e-484f-8f82-b03184ffa975	vale	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
b1d197bc-1d2a-4abd-9f76-f74a96d2f0d1	francklyn	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
27e4f273-f610-4ecb-a638-63442d9458ac	burke	$2a$10$GS0usbIXXc2JbXNHw2KhyuGLuzi3Z00qOFNxaQxsPPvl8vbeHK1bW	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
7c9a8b3a-2eb2-4bde-8a3a-b1cd37938188	lizette	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
ce9b0b34-a119-48be-a6b9-4e4c6d1b8c29	doy	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
903c45a7-8882-44e3-bd04-c41efd9390ec	arnold	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
2536f67a-a501-4365-8145-7dc61e9c5d6c	ephraim	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
fae81e6c-d76d-4cbd-8f81-3a600fead3a3	oona	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
f1195303-4ee5-4480-8166-a6a147343fc4	holt	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
9718adfa-4aa5-4c85-afd6-d8bb09e94f8b	hailee	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
67df578a-d37c-49e0-92e8-25dc64ac74e5	phyllida	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
ab829ad5-1062-4406-986e-36bfd7521aec	nanci	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
c831ece3-eda8-47a7-abd6-bfa1f1f9c2f2	wynn	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
19def74e-c603-4994-8cb0-fc88db944efe	chevalier	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
39e9d4a1-581f-4b63-80a9-45ade51250d0	hilda	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
e59b42c1-37cc-461c-bf3d-459f2bfdcf14	ninette	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
0f0e1a00-1d19-4e44-87fe-84da95fb59ea	lance	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
98297614-4468-4874-8f70-6dcfbfbc98d6	danyelle	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
b387e2d8-41df-4354-b84f-bce64288f638	bethina	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
f1a92f7a-2176-4985-b774-051866707d13	erinn	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
0f9f8c2d-8587-4756-975c-bd9dce558742	robinet	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
ede85a9c-d7cc-4072-a57b-bbe740a2afa0	leann	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
bae53774-05ba-4c25-909d-aa5b10daf278	ewell	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
1d59c7d9-6257-4b2a-aeee-64e76039c199	nikkie	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
90dafb6f-5990-4561-aee8-ebda19d77b37	harmon	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
4cd42df7-0c10-4221-8fd4-e2353c03c04d	redford	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
05bf77a5-aa9e-4ef7-bb5a-894b8810a104	kerry	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
3914ae48-af13-4a42-9abb-e96653880dcb	matilde	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
806d5482-7611-4714-b86f-ffcd81024a64	binni	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
7dd8e168-d369-455e-9f71-299199dbdb31	sherlock	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
f1791838-39f1-4466-a4c9-4c5585d4873c	elwira	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
dd5c7c0c-a390-4dcb-adc5-03c964651003	morty	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
af9772f7-9a95-4e2e-af44-37ee179edde1	rollins	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
62dcda19-b74c-4e0c-ab65-db8aed12e46f	ashli	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
df2ca833-a39b-49ce-b7ec-fc0fd5209727	hali	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
e2b8f32d-a95b-4a75-a101-6d67b52a4a6b	brunhilda	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
d11c85f3-bc3f-40bc-bf3a-b1a7799e6dc6	atlante	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
cdff991c-8a77-4787-a0f8-dc0edcf29420	kalinda	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
681a8e33-dd57-4da6-a281-d31667de4a84	franni	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
a4b3bed5-67fb-4a97-91a6-8f8b1c5902b6	arley	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
67c1c0ee-e8c9-48d8-8696-a2e8be11b0e4	gothart	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
ad283d3b-6a03-40e3-9a11-a068aace1d9b	licha	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
7c145c11-e384-452f-880e-bbfcc3364a1b	halli	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
217da93a-e5b7-4766-b715-f71fcea98f16	viviana	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
479924c0-2ee8-4596-b170-efb702526bd2	hollie	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
19eb6804-cb1d-4c3e-9f44-74465113fd71	wain	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
d80593ff-20bc-43e1-bf8c-cfa0ffda4e2c	hiram	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
7449f3ae-85ef-4835-8ddc-6ec80f620de3	didi	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
8a8904e6-3375-4acd-9b21-171bd21f9ea5	elisha	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
4cc3dffb-4abd-4ac3-b7d6-5fd484e60333	aggy	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
5cf19e3b-b6df-436b-bbf7-5671487d78b7	tate	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
71d27441-c89d-41ed-94cd-37d06b138cc7	crazy	{noop}pass	\N	\N	f	\N	load	2018-07-07 16:40:58.209955	load	2018-07-07 16:40:58.209955
\.


--
-- Data for Name: manager_team; Type: TABLE DATA; Schema: aves; Owner: avesadmin
--

COPY aves.manager_team (id, user_id, team_id, created_by, created_ts, modified_by, modified_ts) FROM stdin;
1	6f5a5ad3-4353-4469-97f7-32d27668b0c7	3	usr	2018-07-17 13:10:31.595145	usr	2018-07-17 13:10:31.595145
\.


--
-- Data for Name: position; Type: TABLE DATA; Schema: aves; Owner: avesadmin
--

COPY aves."position" (id, name, is_deleted, created_by, created_ts, modified_by, modified_ts) FROM stdin;
1	Chief Design Engineer	f	load	2018-07-07 16:28:26.061925	load	2018-07-07 16:28:26.061925
2	Crazy GIT Hater	f	load	2018-07-07 16:28:26.061925	load	2018-07-07 16:28:26.061925
3	Internal Auditor	f	load	2018-07-07 16:28:26.061925	load	2018-07-07 16:28:26.061925
4	Junior Executive	f	load	2018-07-07 16:28:26.061925	load	2018-07-07 16:28:26.061925
5	Legal Assistant	f	load	2018-07-07 16:28:26.061925	load	2018-07-07 16:28:26.061925
6	Operator	f	load	2018-07-07 16:28:26.061925	load	2018-07-07 16:28:26.061925
7	Quality Engineer	f	load	2018-07-07 16:28:26.061925	load	2018-07-07 16:28:26.061925
8	Recruiter	f	load	2018-07-07 16:28:26.061925	load	2018-07-07 16:28:26.061925
9	Sales Representative	f	load	2018-07-07 16:28:26.061925	load	2018-07-07 16:28:26.061925
10	Senior Quality Engineer	f	load	2018-07-07 16:28:26.061925	load	2018-07-07 16:28:26.061925
11	Software Developer	f	load	2018-07-07 16:28:26.061925	load	2018-07-07 16:28:26.061925
12	Structural Engineer	f	load	2018-07-07 16:28:26.061925	load	2018-07-07 16:28:26.061925
\.


--
-- Data for Name: role; Type: TABLE DATA; Schema: aves; Owner: avesadmin
--

COPY aves.role (id, name, description, is_deleted, created_by, created_ts, modified_by, modified_ts) FROM stdin;
1	ROLE_USER	Standard User - Has no admin rights	f	load	2018-07-07 16:07:32.466384	load	2018-07-07 16:07:32.466384
2	ROLE_ADMIN	Admin User - Has permission to perform admin tasks	f	load	2018-07-07 16:07:32.466384	load	2018-07-07 16:07:32.466384
3	ROLE_MANAGER	Manager User - Permission to edit team	f	load	2018-07-07 16:07:33.466384	load	2018-07-07 16:07:33.466384
\.


--
-- Data for Name: status_lookup; Type: TABLE DATA; Schema: aves; Owner: avesadmin
--

COPY aves.status_lookup (id, status, description, is_deleted, created_by, created_ts, modified_by, modified_ts) FROM stdin;
1	ACTIVE	Indicates an active user in the system.	f	load	2018-07-07 16:09:00.211603	load	2018-07-07 16:09:00.211603
2	INACTIVE	Indicates an inactive user in the system.	f	load	2018-07-07 16:09:00.211603	load	2018-07-07 16:09:00.211603
3	PENDING	Indicates a user who is waitinng for activation.	f	load	2018-07-07 16:09:00.211603	load	2018-07-07 16:09:00.211603
\.


--
-- Data for Name: team; Type: TABLE DATA; Schema: aves; Owner: avesadmin
--

COPY aves.team (id, name, parent_team_id, is_deleted, created_by, created_ts, modified_by, modified_ts) FROM stdin;
1	Diospyros ebenum J. Koenig ex Retz.	\N	f	load	2018-07-07 16:11:27.318037	load	2018-07-07 16:11:27.318037
2	Platanthera psycodes (L.) Lindl.	1	f	load	2018-07-07 16:11:27.318037	load	2018-07-07 16:11:27.318037
3	Eriogonum hieraciifolium Benth.	1	f	load	2018-07-07 16:11:27.318037	load	2018-07-07 16:11:27.318037
4	Arisaema triphyllum (L.) Schott	1	f	load	2018-07-07 16:11:27.318037	load	2018-07-07 16:11:27.318037
5	Vaccinium angustifolium Aiton	\N	f	load	2018-07-07 16:11:27.318037	load	2018-07-07 16:11:27.318037
6	Maranta gibba Sm.	5	f	load	2018-07-07 16:11:27.318037	load	2018-07-07 16:11:27.318037
7	Erica cinerea L.	5	f	load	2018-07-07 16:11:27.318037	load	2018-07-07 16:11:27.318037
8	Nephroma isidiosum (Nyl.) Gyel.	\N	f	load	2018-07-07 16:11:27.318037	load	2018-07-07 16:11:27.318037
9	Lythrum tribracteatum Salzm. ex Spreng.	8	f	load	2018-07-07 16:11:27.318037	load	2018-07-07 16:11:27.318037
10	Chaenactis carphoclinia A. Gray	8	f	load	2018-07-07 16:11:27.318037	load	2018-07-07 16:11:27.318037
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: aves; Owner: avesadmin
--

COPY aves."user" (id, first_name, last_name, nick_name, email, phone, subteam_id, location_id, company_id, position_id, is_free_agent, available_date, is_deleted, deleted_by_user_id, created_by, created_ts, modified_by, modified_ts) FROM stdin;
71d27441-c89d-41ed-94cd-37d06b138cc7	Crazy	James	\N	crazy_jamesh@gmail.com	091-291-3892	\N	\N	4	2	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 16:41:22.006712
0003fd6a-1cac-4a00-a4e5-8ac6997da302	Corena	Soonhouse	\N	csoonhouse0@vinaora.com	341-489-6806	2	\N	2	7	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
b4f3ac6a-5531-42ef-a4b7-e648ef02788b	Theodora	Roscher	\N	troscher1@usa.gov	256-956-3036	3	\N	3	9	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
2b726fc3-8d3f-41eb-bb11-2eeb384b36c9	Ely	Ellum	\N	eellum2@indiegogo.com	605-863-3371	4	\N	4	10	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
41196387-0ab2-4c29-90b4-a5111525826d	Buddy	Solly	\N	bsolly3@dailymotion.com	662-669-0332	6	\N	1	4	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
7b2e2138-459d-469c-ad85-35d4a292244f	Thor	Tummasutti	\N	ttummasutti4@cnet.com	625-623-4316	7	\N	7	8	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
ee965630-16eb-4cfd-bb60-ab1bff3c4744	Nealson	Hymus	\N	nhymus5@cdbaby.com	948-565-8605	9	\N	10	6	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
6f5a5ad3-4353-4469-97f7-32d27668b0c7	Zea	Klosser	\N	zklosser6@toplist.cz	379-840-5012	10	\N	9	1	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
6de53471-6947-4024-942f-fe1c7abd6d7e	Samson	Jentges	\N	sjentges7@g.co	802-230-1754	2	\N	5	3	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
248e550c-a896-4736-9a99-e031896d93ac	Corbett	Godbald	\N	cgodbald8@woothemes.com	776-841-9634	3	\N	6	5	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
4fbbc4db-4c7e-484f-8f82-b03184ffa975	Vale	Gooderson	\N	vgooderson9@jalbum.net	921-456-4899	2	\N	8	12	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
b1d197bc-1d2a-4abd-9f76-f74a96d2f0d1	Francklyn	Aslam	\N	faslam0@comsenz.com	163-484-8079	2	\N	7	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
27e4f273-f610-4ecb-a638-63442d9458ac	Burke	Lilburne	\N	blilburne1@foxnews.com	440-834-4871	2	\N	3	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
7c9a8b3a-2eb2-4bde-8a3a-b1cd37938188	Lizette	Point	\N	lpoint2@scribd.com	792-865-7565	3	\N	6	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
ce9b0b34-a119-48be-a6b9-4e4c6d1b8c29	Doy	Ruit	\N	druit3@1und1.de	204-382-3337	4	\N	5	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
903c45a7-8882-44e3-bd04-c41efd9390ec	Arnold	Latour	\N	alatour4@weibo.com	863-465-0729	6	\N	10	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
2536f67a-a501-4365-8145-7dc61e9c5d6c	Ephraim	Rockcliff	\N	erockcliff5@merriam-webster.com	321-295-2572	6	\N	9	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
fae81e6c-d76d-4cbd-8f81-3a600fead3a3	Oona	Berzen	\N	oberzen6@t-online.de	700-307-4730	7	\N	1	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
f1195303-4ee5-4480-8166-a6a147343fc4	Holt	Pothecary	\N	hpothecary7@netscape.com	928-883-5330	9	\N	6	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
9718adfa-4aa5-4c85-afd6-d8bb09e94f8b	Hailee	O'Dowgaine	\N	hodowgaine8@telegraph.co.uk	893-312-2314	9	\N	4	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
67df578a-d37c-49e0-92e8-25dc64ac74e5	Phyllida	Spridgen	\N	pspridgen9@java.com	999-619-4959	10	\N	6	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
ab829ad5-1062-4406-986e-36bfd7521aec	Nanci	Leander	\N	nleandera@china.com.cn	226-288-9848	2	\N	7	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
c831ece3-eda8-47a7-abd6-bfa1f1f9c2f2	Wynn	Costigan	\N	wcostiganb@paypal.com	655-138-1232	2	\N	3	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
19def74e-c603-4994-8cb0-fc88db944efe	Chevalier	Amberson	\N	cambersonc@hugedomains.com	782-106-4058	3	\N	5	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
39e9d4a1-581f-4b63-80a9-45ade51250d0	Hilda	Inchan	\N	hinchand@washington.edu	844-201-0151	4	\N	5	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
e59b42c1-37cc-461c-bf3d-459f2bfdcf14	Ninette	Coetzee	\N	ncoetzeee@reuters.com	260-183-1500	6	\N	10	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
0f0e1a00-1d19-4e44-87fe-84da95fb59ea	Lance	Lissett	\N	llissettf@gmpg.org	415-184-8202	6	\N	3	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
98297614-4468-4874-8f70-6dcfbfbc98d6	Danyelle	Metts	\N	dmettsg@hostgator.com	713-830-4958	7	\N	3	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
b387e2d8-41df-4354-b84f-bce64288f638	Bethina	Cadore	\N	bcadoreh@xing.com	273-754-8347	9	\N	7	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
f1a92f7a-2176-4985-b774-051866707d13	Erinn	Burniston	\N	eburnistoni@irs.gov	212-829-5276	9	\N	6	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
0f9f8c2d-8587-4756-975c-bd9dce558742	Robinet	Beardwood	\N	rbeardwoodj@ft.com	732-790-4133	10	\N	9	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
ede85a9c-d7cc-4072-a57b-bbe740a2afa0	Leann	Vanner	\N	lvannerk@php.net	160-380-7775	2	\N	6	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
bae53774-05ba-4c25-909d-aa5b10daf278	Ewell	Lubomirski	\N	elubomirskil@webs.com	467-459-4771	2	\N	10	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
1d59c7d9-6257-4b2a-aeee-64e76039c199	Nikkie	Cocke	\N	ncockem@miitbeian.gov.cn	235-191-4082	3	\N	2	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
90dafb6f-5990-4561-aee8-ebda19d77b37	Harmon	Algeo	\N	halgeon@sfgate.com	687-244-4661	4	\N	4	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
4cd42df7-0c10-4221-8fd4-e2353c03c04d	Redford	Melmoth	\N	rmelmotho@reverbnation.com	397-627-7103	6	\N	5	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
05bf77a5-aa9e-4ef7-bb5a-894b8810a104	Kerry	Stiling	\N	kstilingp@fastcompany.com	898-969-7724	6	\N	6	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
3914ae48-af13-4a42-9abb-e96653880dcb	Matilde	Seaborne	\N	mseaborneq@bing.com	858-327-4448	7	\N	7	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
806d5482-7611-4714-b86f-ffcd81024a64	Binni	Claasen	\N	bclaasenr@va.gov	718-611-4832	9	\N	7	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
7dd8e168-d369-455e-9f71-299199dbdb31	Sherlock	Marking	\N	smarkings@soundcloud.com	558-892-2292	9	\N	5	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
f1791838-39f1-4466-a4c9-4c5585d4873c	Elwira	Saph	\N	esapht@bloglovin.com	794-646-8961	10	\N	4	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
dd5c7c0c-a390-4dcb-adc5-03c964651003	Morty	Banck	\N	mbancku@google.de	536-237-9579	2	\N	2	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
af9772f7-9a95-4e2e-af44-37ee179edde1	Rollins	Pudsey	\N	rpudseyv@nifty.com	171-331-7157	2	\N	6	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
62dcda19-b74c-4e0c-ab65-db8aed12e46f	Ashli	Haughan	\N	ahaughanw@trellian.com	360-298-3491	3	\N	7	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
df2ca833-a39b-49ce-b7ec-fc0fd5209727	Hali	O'Shea	\N	hosheax@free.fr	332-844-5968	4	\N	4	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
e2b8f32d-a95b-4a75-a101-6d67b52a4a6b	Brunhilda	Tourmell	\N	btourmelly@friendfeed.com	875-568-1265	6	\N	3	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
d11c85f3-bc3f-40bc-bf3a-b1a7799e6dc6	Atlante	Klarzynski	\N	aklarzynskiz@army.mil	335-185-3025	6	\N	5	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
cdff991c-8a77-4787-a0f8-dc0edcf29420	Kalinda	Nel	\N	knel10@google.de	696-964-0617	7	\N	2	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
681a8e33-dd57-4da6-a281-d31667de4a84	Franni	Whittuck	\N	fwhittuck11@oracle.com	695-490-5965	9	\N	1	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
a4b3bed5-67fb-4a97-91a6-8f8b1c5902b6	Arley	Swarbrick	\N	aswarbrick12@tamu.edu	675-238-2608	9	\N	3	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
67c1c0ee-e8c9-48d8-8696-a2e8be11b0e4	Gothart	Daborne	\N	gdaborne13@51.la	472-131-9269	10	\N	10	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
ad283d3b-6a03-40e3-9a11-a068aace1d9b	Licha	Gilgryst	\N	lgilgryst14@example.com	221-102-2236	2	\N	7	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
7c145c11-e384-452f-880e-bbfcc3364a1b	Halli	Tuley	\N	htuley15@photobucket.com	507-886-0401	2	\N	4	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
217da93a-e5b7-4766-b715-f71fcea98f16	Viviana	Shilling	\N	vshilling16@biglobe.ne.jp	627-717-6539	3	\N	5	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
479924c0-2ee8-4596-b170-efb702526bd2	Hollie	Whatham	\N	hwhatham17@google.com.hk	217-655-1829	4	\N	9	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
19eb6804-cb1d-4c3e-9f44-74465113fd71	Wain	Crooke	\N	wcrooke18@theglobeandmail.com	790-221-8951	6	\N	2	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
d80593ff-20bc-43e1-bf8c-cfa0ffda4e2c	Hiram	Bagnell	\N	hbagnell19@barnesandnoble.com	738-305-5300	6	\N	9	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
7449f3ae-85ef-4835-8ddc-6ec80f620de3	Didi	Megany	\N	dmegany1a@ehow.com	238-569-7131	7	\N	2	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
8a8904e6-3375-4acd-9b21-171bd21f9ea5	Elisha	McKerron	\N	emckerron1b@weather.com	982-571-5165	9	\N	1	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
4cc3dffb-4abd-4ac3-b7d6-5fd484e60333	Aggy	Rees	\N	arees1c@nasa.gov	387-150-3145	9	\N	9	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
5cf19e3b-b6df-436b-bbf7-5671487d78b7	Tate	Peat	\N	tpeat1d@pen.io	127-421-0269	10	\N	1	11	f	\N	f	\N	load	2018-07-07 16:41:22.006712	load	2018-07-07 17:54:08.602748
\.


--
-- Data for Name: user_activation; Type: TABLE DATA; Schema: aves; Owner: avesadmin
--

COPY aves.user_activation (id, user_id, verification_hash, created_by, created_ts, modified_by, modified_ts) FROM stdin;
\.


--
-- Data for Name: user_role; Type: TABLE DATA; Schema: aves; Owner: avesadmin
--

COPY aves.user_role (id, user_id, role_id, created_by, created_ts, modified_by, modified_ts) FROM stdin;
1	27e4f273-f610-4ecb-a638-63442d9458ac	1	usr	2018-07-17 16:01:38.553434	usr	2018-07-17 16:01:38.553434
2	2b726fc3-8d3f-41eb-bb11-2eeb384b36c9	2	usr	2018-07-17 16:01:38.553434	usr	2018-07-17 16:01:38.553434
3	6f5a5ad3-4353-4469-97f7-32d27668b0c7	3	usr	2018-07-17 16:01:38.553434	usr	2018-07-17 16:01:38.553434
\.


--
-- Name: company_id_seq; Type: SEQUENCE SET; Schema: aves; Owner: avesadmin
--

SELECT setval('aves.company_id_seq', COALESCE((SELECT MAX(id)+1 FROM aves.company), 1), false);


--
-- Name: location_id_seq; Type: SEQUENCE SET; Schema: aves; Owner: avesadmin
--

SELECT pg_catalog.setval('aves.location_id_seq', COALESCE((SELECT MAX(id)+1 FROM aves.location), 1), false);


--
-- Name: manager_team_id_seq; Type: SEQUENCE SET; Schema: aves; Owner: avesadmin
--

SELECT pg_catalog.setval('aves.manager_team_id_seq', COALESCE((SELECT MAX(id)+1 FROM aves.manager_team), 1), false);


--
-- Name: position_id_seq; Type: SEQUENCE SET; Schema: aves; Owner: avesadmin
--

SELECT pg_catalog.setval('aves.position_id_seq', COALESCE((SELECT MAX(id)+1 FROM aves.position), 1), true);


--
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: aves; Owner: avesadmin
--

SELECT pg_catalog.setval('aves.role_id_seq', COALESCE((SELECT MAX(id)+1 FROM aves.role), 1), false);


--
-- Name: status_lookup_id_seq; Type: SEQUENCE SET; Schema: aves; Owner: avesadmin
--

SELECT pg_catalog.setval('aves.status_lookup_id_seq', COALESCE((SELECT MAX(id)+1 FROM aves.status_lookup), 1), false);


--
-- Name: team_id_seq; Type: SEQUENCE SET; Schema: aves; Owner: avesadmin
--

SELECT pg_catalog.setval('aves.team_id_seq', COALESCE((SELECT MAX(id)+1 FROM aves.team), 1), false);


--
-- Name: user_activation_id_seq; Type: SEQUENCE SET; Schema: aves; Owner: avesadmin
--

SELECT pg_catalog.setval('aves.user_activation_id_seq', COALESCE((SELECT MAX(id)+1 FROM aves.user_activation), 1), false);


--
-- Name: user_role_id_seq; Type: SEQUENCE SET; Schema: aves; Owner: avesadmin
--

SELECT pg_catalog.setval('aves.user_role_id_seq', COALESCE((SELECT MAX(id)+1 FROM aves.user_role), 1), false);


--
-- Name: company company_pkey; Type: CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.company
    ADD CONSTRAINT company_pkey PRIMARY KEY (id);


--
-- Name: company company_un; Type: CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.company
    ADD CONSTRAINT company_un UNIQUE (name);


--
-- Name: location location_pkey; Type: CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.location
    ADD CONSTRAINT location_pkey PRIMARY KEY (id);


--
-- Name: location location_un; Type: CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.location
    ADD CONSTRAINT location_un UNIQUE (name);


--
-- Name: manager_team man_team_unique; Type: CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.manager_team
    ADD CONSTRAINT man_team_unique UNIQUE (user_id, team_id);


--
-- Name: manager_team mtct_pkey; Type: CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.manager_team
    ADD CONSTRAINT mtct_pkey PRIMARY KEY (id);


--
-- Name: login name; Type: CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.login
    ADD CONSTRAINT name UNIQUE (username);


--
-- Name: position position_pkey; Type: CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves."position"
    ADD CONSTRAINT position_pkey PRIMARY KEY (id);


--
-- Name: position position_un; Type: CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves."position"
    ADD CONSTRAINT position_un UNIQUE (name);


--
-- Name: role prole_pkey; Type: CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.role
    ADD CONSTRAINT prole_pkey PRIMARY KEY (id);


--
-- Name: role role_un; Type: CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.role
    ADD CONSTRAINT role_un UNIQUE (name);


--
-- Name: status_lookup sl_pkey; Type: CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.status_lookup
    ADD CONSTRAINT sl_pkey PRIMARY KEY (id);


--
-- Name: team team_pkey; Type: CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.team
    ADD CONSTRAINT team_pkey PRIMARY KEY (id);


--
-- Name: team team_un; Type: CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.team
    ADD CONSTRAINT team_un UNIQUE (name);


--
-- Name: user_activation uact_pkey; Type: CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.user_activation
    ADD CONSTRAINT uact_pkey PRIMARY KEY (id);


--
-- Name: user_role urct_pkey; Type: CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.user_role
    ADD CONSTRAINT urct_pkey PRIMARY KEY (id);


--
-- Name: user user_email_key; Type: CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves."user"
    ADD CONSTRAINT user_email_key UNIQUE (email);


--
-- Name: user user_id_key; Type: CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves."user"
    ADD CONSTRAINT user_id_key UNIQUE (id);


--
-- Name: login user_pkey; Type: CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.login
    ADD CONSTRAINT user_pkey PRIMARY KEY (user_id);


--
-- Name: user_role user_role_unique; Type: CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.user_role
    ADD CONSTRAINT user_role_unique UNIQUE (user_id, role_id);


--
-- Name: company update_company_modts; Type: TRIGGER; Schema: aves; Owner: avesadmin
--

CREATE TRIGGER update_company_modts BEFORE UPDATE ON aves.company FOR EACH ROW EXECUTE PROCEDURE aves.update_modified_column();


--
-- Name: location update_location_modts; Type: TRIGGER; Schema: aves; Owner: avesadmin
--

CREATE TRIGGER update_location_modts BEFORE UPDATE ON aves.location FOR EACH ROW EXECUTE PROCEDURE aves.update_modified_column();


--
-- Name: position update_position_modts; Type: TRIGGER; Schema: aves; Owner: avesadmin
--

CREATE TRIGGER update_position_modts BEFORE UPDATE ON aves."position" FOR EACH ROW EXECUTE PROCEDURE aves.update_modified_column();


--
-- Name: role update_role_modts; Type: TRIGGER; Schema: aves; Owner: avesadmin
--

CREATE TRIGGER update_role_modts BEFORE UPDATE ON aves.role FOR EACH ROW EXECUTE PROCEDURE aves.update_modified_column();


--
-- Name: status_lookup update_status_lookup_modts; Type: TRIGGER; Schema: aves; Owner: avesadmin
--

CREATE TRIGGER update_status_lookup_modts BEFORE UPDATE ON aves.status_lookup FOR EACH ROW EXECUTE PROCEDURE aves.update_modified_column();


--
-- Name: team update_team_modts; Type: TRIGGER; Schema: aves; Owner: avesadmin
--

CREATE TRIGGER update_team_modts BEFORE UPDATE ON aves.team FOR EACH ROW EXECUTE PROCEDURE aves.update_modified_column();


--
-- Name: user_activation update_user_activation_modts; Type: TRIGGER; Schema: aves; Owner: avesadmin
--

CREATE TRIGGER update_user_activation_modts BEFORE UPDATE ON aves.user_activation FOR EACH ROW EXECUTE PROCEDURE aves.update_modified_column();


--
-- Name: user update_user_info_modts; Type: TRIGGER; Schema: aves; Owner: avesadmin
--

CREATE TRIGGER update_user_info_modts BEFORE UPDATE ON aves."user" FOR EACH ROW EXECUTE PROCEDURE aves.update_modified_column();


--
-- Name: login update_user_modts; Type: TRIGGER; Schema: aves; Owner: avesadmin
--

CREATE TRIGGER update_user_modts BEFORE UPDATE ON aves.login FOR EACH ROW EXECUTE PROCEDURE aves.update_modified_column();

CREATE TRIGGER update_user_role_modts BEFORE UPDATE ON aves.user_role FOR EACH ROW EXECUTE PROCEDURE aves.update_modified_column();

CREATE TRIGGER update_manager_team_modts BEFORE UPDATE ON aves.manager_team FOR EACH ROW EXECUTE PROCEDURE aves.update_modified_column();


--
-- Name: login l_user_id_fk; Type: FK CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.login
    ADD CONSTRAINT l_user_id_fk FOREIGN KEY (user_id) REFERENCES aves."user"(id);


--
-- Name: manager_team mt_team_id_fk; Type: FK CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.manager_team
    ADD CONSTRAINT mt_team_id_fk FOREIGN KEY (team_id) REFERENCES aves.team(id);


--
-- Name: manager_team mt_userid_fk; Type: FK CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.manager_team
    ADD CONSTRAINT mt_userid_fk FOREIGN KEY (user_id) REFERENCES aves."user"(id);


--
-- Name: login u_status_id_fk; Type: FK CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.login
    ADD CONSTRAINT u_status_id_fk FOREIGN KEY (status_id) REFERENCES aves.status_lookup(id);


--
-- Name: user_activation ua_user_id_fk; Type: FK CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.user_activation
    ADD CONSTRAINT ua_user_id_fk FOREIGN KEY (user_id) REFERENCES aves."user"(id);


--
-- Name: user ui_company_id_fk; Type: FK CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves."user"
    ADD CONSTRAINT ui_company_id_fk FOREIGN KEY (company_id) REFERENCES aves.company(id);


--
-- Name: user ui_location_id_fk; Type: FK CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves."user"
    ADD CONSTRAINT ui_location_id_fk FOREIGN KEY (location_id) REFERENCES aves.company(id);


--
-- Name: user ui_position_id_fk; Type: FK CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves."user"
    ADD CONSTRAINT ui_position_id_fk FOREIGN KEY (position_id) REFERENCES aves."position"(id);


--
-- Name: user ui_team_id_fk; Type: FK CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves."user"
    ADD CONSTRAINT ui_team_id_fk FOREIGN KEY (subteam_id) REFERENCES aves.team(id);


--
-- Name: user_role ur_role_id_fk; Type: FK CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.user_role
    ADD CONSTRAINT ur_role_id_fk FOREIGN KEY (role_id) REFERENCES aves.role(id);


--
-- Name: user_role ur_user_id_fk; Type: FK CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves.user_role
    ADD CONSTRAINT ur_user_id_fk FOREIGN KEY (user_id) REFERENCES aves."user"(id);


--
-- Name: user user_info_location_fk; Type: FK CONSTRAINT; Schema: aves; Owner: avesadmin
--

ALTER TABLE ONLY aves."user"
    ADD CONSTRAINT user_info_location_fk FOREIGN KEY (location_id) REFERENCES aves.location(id);


--
-- PostgreSQL database dump complete
--

