package com.deea.aves.userrole;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;


@RestController
@RequestMapping("/userroles")
@ResponseStatus(HttpStatus.OK)
public class UserRoleController {

    @Autowired
    UserRoleService userRoleService;

    @GetMapping
    public List<UserRole> getAllUserRole() {
        return userRoleService.findAll();
    }

    @GetMapping("{id}")
    public UserRole getById(HttpServletResponse response, @PathVariable("id") Long id) {
        return userRoleService.findById(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public String insert(@RequestBody UserRole userRole) {
        // TODO to return url to entity.
        userRoleService.insert(userRole);
        return "ok";
    }

    @PutMapping("{id}")
    public String update(@RequestBody UserRole userRole) {
         // TODO to return url to entity.
        userRoleService.update(userRole);
        return "ok";
    }

    @DeleteMapping("{id}")
    public String delete(@PathVariable("id") Long id) {
        userRoleService.delete(id);
        return "ok";
    }

}