package com.deea.aves.userrole;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
@Table(name="user_role")
public class UserRole {
   
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="user_id")
    UUID userId;

    @Column(name="role_id")
    private Long roleId;

    @Override
    public String toString() {
        return String.format(
                "UserRole[id=%d, user_id='%s', team_id=%d]",
                id, userId, roleId);
    }
}