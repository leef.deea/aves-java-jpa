package com.deea.aves.userrole;

import java.util.List;
import java.util.Optional;

import com.deea.aves.exception.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRoleService {

    @Autowired
    UserRoleRepository managerTeamRepository;

    public List<UserRole> findAll() {
        return managerTeamRepository.findAll();
    }

    public UserRole findById( Long id ) {
        return managerTeamRepository
            .findById(id)
            .orElseThrow(()->new NotFoundException("No UserRole with id "+id ));
    }

    public Long insert( UserRole managerTeam ) {
        return managerTeamRepository.save(managerTeam).getId();
    }

    public void update( UserRole managerTeam ) {
        managerTeamRepository.save(managerTeam);
    }


    public String delete( Long id ) {
        
        return "not found";
    }

      
}