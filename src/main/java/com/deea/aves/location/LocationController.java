package com.deea.aves.location;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;


@RestController
@RequestMapping("/locations")
@ResponseStatus(HttpStatus.OK)
public class LocationController {

    @Autowired
    LocationService locationService;

    @GetMapping
    public List<Location> getAllLocation() {
        return locationService.findAll();
    }

    @GetMapping("{id}")
    public Location getById(HttpServletResponse response, @PathVariable("id") Long id) {
        return locationService.findById(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public String insert(@RequestBody Location location) {
        // TODO to return url to entity.
        locationService.insert(location);
        return "ok";
    }

    @PutMapping("{id}")
    public String update(@RequestBody Location location) {
         // TODO to return url to entity.
        locationService.update(location);
        return "ok";
    }

    @DeleteMapping("{id}")
    public String delete(@PathVariable("id") Long id) {
        locationService.delete(id);
        return "ok";
    }

}