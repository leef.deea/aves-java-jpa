package com.deea.aves.location;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
@Table(name="location")
public class Location {
   
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotBlank(message = "Name cannot be blank")	
    @Column(name="name")
    private String name;


    @Column(name="address_line_1")
    private String addressLine1;

    @Column(name="address_line_2")
    private String addressLine2;

    @Column(name="city")
    private String City;

    @Column(name="state")
    private String State;

    @Column(name="zipcode")
    private String Zipcode;

    @Column(name="is_deleted")
    private boolean isDeleted;

    @Column(name="created_by")
    private String createdBy;

    @Column(name="created_ts")
    private Date createdTs;
    
    @Column(name="modified_by")
    private String modifiedBy;

    @Column(name="modified_ts")
    private Date modifiedTs;


    @Override
    public String toString() {
        return String.format(
                "Location[id=%d, name='%s', deleted=%s]",
                id, name, isDeleted);
    }
}