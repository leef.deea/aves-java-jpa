package com.deea.aves.managerteam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;


@RestController
@RequestMapping("/managerteams")
@ResponseStatus(HttpStatus.OK)
public class ManagerTeamController {

    @Autowired
    ManagerTeamService managerTeamService;

    @GetMapping
    public List<ManagerTeam> getAllManagerTeam() {
        return managerTeamService.findAll();
    }

    @GetMapping("{id}")
    public ManagerTeam getById(HttpServletResponse response, @PathVariable("id") Long id) {
        return managerTeamService.findById(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public String insert(@RequestBody ManagerTeam managerTeam) {
        // TODO to return url to entity.
        managerTeamService.insert(managerTeam);
        return "ok";
    }

    @PutMapping("{id}")
    public String update(@RequestBody ManagerTeam managerTeam) {
         // TODO to return url to entity.
        managerTeamService.update(managerTeam);
        return "ok";
    }

    @DeleteMapping("{id}")
    public String delete(@PathVariable("id") Long id) {
        managerTeamService.delete(id);
        return "ok";
    }

}