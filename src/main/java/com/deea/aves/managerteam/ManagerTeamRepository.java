package com.deea.aves.managerteam;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;



@Repository
public interface ManagerTeamRepository extends JpaRepository<ManagerTeam, Long> {

    List<ManagerTeam> findByUserId(UUID user_id);

    List<ManagerTeam> findAll();

    Optional<ManagerTeam> findById(Long id);

}