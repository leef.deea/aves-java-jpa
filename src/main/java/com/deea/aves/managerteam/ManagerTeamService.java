package com.deea.aves.managerteam;

import java.util.List;
import java.util.Optional;

import com.deea.aves.exception.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ManagerTeamService {

    @Autowired
    ManagerTeamRepository managerTeamRepository;

    public List<ManagerTeam> findAll() {
        return managerTeamRepository.findAll();
    }

    public ManagerTeam findById( Long id ) {
        return managerTeamRepository
            .findById(id)
            .orElseThrow(()->new NotFoundException("No ManagerTeam with id "+id ));
    }

    public Long insert( ManagerTeam managerTeam ) {
        return managerTeamRepository.save(managerTeam).getId();
    }

    public void update( ManagerTeam managerTeam ) {
        managerTeamRepository.save(managerTeam);
    }


    public String delete( Long id ) {
        
        return "not found";
    }

      
}