package com.deea.aves.managerteam;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
@Table(name="manager_team")
public class ManagerTeam {
   
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="user_id")
    UUID userId;

    @Column(name="team_id")
    private Long teamId;

    @Override
    public String toString() {
        return String.format(
                "UserRole[id=%d, user_id='%s', team_id=%d]",
                id, userId, teamId);
    }
}