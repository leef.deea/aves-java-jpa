package com.deea.aves.role;

import java.util.List;
import java.util.Optional;

import com.deea.aves.exception.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {

    @Autowired
    RoleRepository roleRepository;

    public List<Role> findAll() {
        return roleRepository.findAll();
    }

    public Role findById( Long id ) {
        return roleRepository
            .findById(id)
            .orElseThrow(()->new NotFoundException("No Role with id "+id ));
    }

    public Long insert( Role role ) {
        return roleRepository.save(role).getId();
    }

    public void update( Role role ) {
        roleRepository.save(role);
    }


    public String delete( Long id ) {
        
        Optional<Role> role = roleRepository.findById(id);
        if ( role.isPresent() ) {
            Role c = role.get();
            c.setDeleted(true);
            roleRepository.save(c);
            return "ok";
        }
        return "not found";
    }

      
}