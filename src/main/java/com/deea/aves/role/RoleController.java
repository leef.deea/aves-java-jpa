package com.deea.aves.role;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;


@RestController
@RequestMapping("/roles")
@ResponseStatus(HttpStatus.OK)
public class RoleController {

    @Autowired
    RoleService roleService;

    @GetMapping
    public List<Role> getAllRole() {
        return roleService.findAll();
    }

    @GetMapping("{id}")
    public Role getById(HttpServletResponse response, @PathVariable("id") Long id) {
        return roleService.findById(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public String insert(@RequestBody Role role) {
        // TODO to return url to entity.
        roleService.insert(role);
        return "ok";
    }

    @PutMapping("{id}")
    public String update(@RequestBody Role role) {
         // TODO to return url to entity.
        roleService.update(role);
        return "ok";
    }

    @DeleteMapping("{id}")
    public String delete(@PathVariable("id") Long id) {
        roleService.delete(id);
        return "ok";
    }

}