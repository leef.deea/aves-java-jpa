package com.deea.aves.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.UUID;



@RestController
@RequestMapping("/users")
@ResponseStatus(HttpStatus.OK)

public class UserController {

    @Autowired
    UserRepository userInfoRepository;

    @Autowired
    UserService userInfoService;

    @GetMapping()
    public List<User> getAll() {
        return userInfoService.findAll();
    }

    @GetMapping("{id}")
    public User getById(@PathVariable("id") UUID id) {
        return userInfoService.findById(id);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping()
    public User insert(@RequestBody User userInfo) {
        userInfoService.save(userInfo);
        return userInfo;
    }

    @PutMapping("{id}")
    public String update(@RequestBody User userInfo) {
         // TODO to return url to entity.
        userInfoService.save(userInfo);
        return "ok";
    }

    
    @DeleteMapping("{id}")
    public String delete(@PathVariable UUID id) {
         // TODO to return url to entity.
        userInfoService.deleteUser(id);
        return "ok";
    }
}