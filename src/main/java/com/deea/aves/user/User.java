package com.deea.aves.user;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.deea.aves.company.Company;
import com.deea.aves.location.Location;
import com.deea.aves.position.Position;
import com.deea.aves.team.Team;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
@Table(name="user")
public class User {
   
    @Id
    @Column(name="id")
    private UUID id;

    @Column(name="first_name")
    private String firstName;

    @Column(name="last_name")
    private String lastName;

    @Column(name="nick_name")
    private String nickName;

    private String email;

    private String phone;

    // @Column(name="subteam_id")
    // private String subTeamId;

    // @Column(name="location_id")
    // private String locationId;

    // @Column(name="company_id")
    // private Long companyId;

    // @Column(name="position_id")
    // private long positionId;

    @Column(name="is_free_agent")
    private boolean isFreeAgent;

    @Column(name="available_date")
    private Date availableDate;

    @Column(name="is_deleted")
    private boolean isDeleted;

    @Column(name="deleted_by_user_id")
    private UUID deletedByUserId;

    @OneToOne()
    @JoinColumn(name = "company_id")
    private Company company; 

    @OneToOne()
    @JoinColumn(name = "location_id")
    private Location location; 

    @OneToOne()
    @JoinColumn(name = "position_id")
    private Position position; 
        
    @OneToOne()
    @JoinColumn(name = "subteam_id")
    private Team subTeam; 

    @Transient
    private boolean isEditable = false;

    @Transient
    private boolean isDeletable = false;

    @Override
    public String toString() {
        return String.format(
            "UserInfo[id=%d, name='%s %s', email='%s', phone='%s',"+
            " location='%s', company_id= %d, position='%s', deleted=%s]",
            id, firstName, lastName, email, phone,
            location.getName(), company.getName(), position.getName(), isDeleted
        );
    }
}