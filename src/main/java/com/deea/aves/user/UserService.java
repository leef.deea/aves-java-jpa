package com.deea.aves.user;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.plaf.InsetsUIResource;

import com.deea.aves.company.CompanyRepository;
import com.deea.aves.exception.NotFoundException;
import com.deea.aves.login.Login;
import com.deea.aves.login.LoginRepository;
import com.deea.aves.team.TeamRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    TeamRepository teamRepository;

    @Autowired
    LoginRepository LoginRepository;


    @Autowired
    private HttpServletRequest request; 

    @Autowired
    protected HttpServletResponse response;

    public List<User> findAll() {

        List<User> users = userRepository.findAll();
        setUserDeletableEditable(users);
        return users;

    }
    
    public User findById(UUID id) {

        Optional<User> user = userRepository.findById(id);
        if ( user.isPresent()) {
            User u = user.get();
            setUserDeletableEditable(u);
            return u;
        }
        throw new NotFoundException("No User with id = " + id );

    }

    public User save(User user) {
        userRepository.save(user);
        return user;
    }

    private void setUserDeletableEditable( User inusers ) {

        List<User> userList = new ArrayList<User>();
        userList.add((User)inusers);
        setUserDeletableEditable(userList);
        
    }

    private void setUserDeletableEditable( List<User> userList ) {

        
        // UUID userid= this.getUserId();
        // if ( userid == null ) {
        //     // user not logged in nothing to do so return;
        //     return;
        // }
        
        // Set<Long> managerTeams = new TreeSet<>();
        // if ( request.isUserInRole("MANAGER") ) {
        //     // manager find all the team manager belong to.
        //     List<UserTeam> managerTM=userTeamRepository.findByUserId(userid);
        //     for ( UserTeam tm : managerTM) {
        //         managerTeams.add(tm.getTeamId());
        //     }  
        // }

        // for ( User user : userList) {
        //     if ( request.isUserInRole("ADMIN")) {
        //         user.setDeletable(true);
        //         user.setEditable(true);
        //     } else if ( request.isUserInRole("MANAGER") ) {
        //         List<UserTeam> userTM=userTeamRepository.findByUserId(user.getId());
        //         for ( UserTeam tm : userTM) {
        //             if ( managerTeams.contains(tm.getTeamId()) ) {
        //                 user.setDeletable(true);
        //                 user.setEditable(true);
        //                 break;
        //             }
        //         }
        //     } else {
        //         // standard user lets check to see if user is editing there own profile.
        //         if ( userid.equals(user.getId()) ) {
        //             user.setDeletable(true);
        //             user.setEditable(true);
        //         }
        //     }
        // }

    }

    public UUID getUserId() {

        if ( request.getUserPrincipal() != null ) {
            String username = request.getUserPrincipal().getName();
            Optional<Login> login = LoginRepository.findByUsername(username);
            if ( login.isPresent()) {
                return login.get().getUserId();
            }
        }
        return null;

    }

    public String deleteUser( UUID userid ) {
        
        Optional<User> user = userRepository.findById(userid);
        if ( user.isPresent() ) {
            User u = user.get();
            u.setDeletedByUserId(getUserId());
            u.setDeleted(true);
            userRepository.save(u);
            return "ok";
        }
        return "not found";
    }



}