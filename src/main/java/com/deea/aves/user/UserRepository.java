package com.deea.aves.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Repository
public interface UserRepository extends JpaRepository<User, UUID> {

    Optional<User> findByLastName(String name);
    Optional<User> findByFirstName(String name);

    List<User> findAll();
    List<User> findByPositionId( String position);
    List<User> findByLocationId( String location);

    Optional<User> findById(UUID id);
    


}