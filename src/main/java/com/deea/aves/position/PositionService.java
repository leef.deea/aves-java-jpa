package com.deea.aves.position;

import java.util.List;
import java.util.Optional;

import com.deea.aves.exception.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PositionService {

    @Autowired
    PositionRepository positionRepository;

    public List<Position> findAll() {
        return positionRepository.findAll();
    }

    public Position findById( Long id ) {
        return positionRepository
            .findById(id)
            .orElseThrow(()->new NotFoundException("No Position with id "+id ));
    }

    public Long insert( Position position ) {
        return positionRepository.save(position).getId();
    }

    public void update( Position position ) {
        positionRepository.save(position);
    }


    public String delete( Long id ) {
        
        Optional<Position> position = positionRepository.findById(id);
        if ( position.isPresent() ) {
            Position c = position.get();
            c.setDeleted(true);
            positionRepository.save(c);
            return "ok";
        }
        return "not found";
    }

      
}