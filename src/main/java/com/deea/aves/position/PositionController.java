package com.deea.aves.position;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;


@RestController
@RequestMapping("/positions")
@ResponseStatus(HttpStatus.OK)
public class PositionController {

    @Autowired
    PositionService positionService;

    @GetMapping
    public List<Position> getAllPosition() {
        return positionService.findAll();
    }

    @GetMapping("{id}")
    public Position getById(HttpServletResponse response, @PathVariable("id") Long id) {
        return positionService.findById(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public String insert(@RequestBody Position position) {
        // TODO to return url to entity.
        positionService.insert(position);
        return "ok";
    }

    @PutMapping("{id}")
    public String update(@RequestBody Position position) {
         // TODO to return url to entity.
        positionService.update(position);
        return "ok";
    }

    @DeleteMapping("{id}")
    public String delete(@PathVariable("id") Long id) {
        positionService.delete(id);
        return "ok";
    }

}