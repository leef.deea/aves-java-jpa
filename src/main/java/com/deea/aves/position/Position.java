package com.deea.aves.position;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
@Table(name="position")
public class Position {
   
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotBlank(message = "Name cannot be blank")	
    @Column(name="name")
    private String name;

    @Column(name="is_deleted")
    private boolean isDeleted;

    @Column(name="created_by")
    private String createdBy;

    @Column(name="created_ts")
    private Date createdTs;
    
    @Column(name="modified_by")
    private String modifiedBy;

    @Column(name="modified_ts")
    private Date modifiedTs;


    @Override
    public String toString() {
        return String.format(
                "Position[id=%d, name='%s', deleted=%s]",
                id, name, isDeleted);
    }
}