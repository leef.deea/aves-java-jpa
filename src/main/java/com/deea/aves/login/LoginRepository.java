package com.deea.aves.login;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Repository
public interface LoginRepository extends JpaRepository<Login, UUID> {

    Optional<Login> findByUsername(String username);
    List<Login> findAll();

}