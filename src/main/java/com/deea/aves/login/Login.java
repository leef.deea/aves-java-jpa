package com.deea.aves.login;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
@Table(name="login")
public class Login {
   
    @Id
    @Column(name="user_id")
    private UUID userId;

    @Column(name="username")
    private String username;

    private String password;
 

    @Override
    public String toString() {
        return String.format(
                "Company[id=%d, name='%s', role='%s']",
                userId, username);
    }
}