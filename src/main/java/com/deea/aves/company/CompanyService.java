package com.deea.aves.company;

import java.util.List;
import java.util.Optional;

import com.deea.aves.exception.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompanyService {

    @Autowired
    CompanyRepository companyRepository;

    public List<Company> findAll() {
        return companyRepository.findAll();
    }

    public Company findById( Long id ) {
        return companyRepository
            .findById(id)
            .orElseThrow(()->new NotFoundException("No Company with id "+id ));
    }

    public Long insert( Company company ) {
        return companyRepository.save(company).getId();
    }

    public void update( Company company ) {
        companyRepository.save(company);
    }


    public String delete( Long id ) {
        
        Optional<Company> company = companyRepository.findById(id);
        if ( company.isPresent() ) {
            Company c = company.get();
            c.setDeleted(true);
            companyRepository.save(c);
            return "ok";
        }
        return "not found";
    }

      
}