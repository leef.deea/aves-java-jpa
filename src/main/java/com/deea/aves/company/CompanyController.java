package com.deea.aves.company;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;


@RestController
@RequestMapping("/companies")
@ResponseStatus(HttpStatus.OK)
public class CompanyController {

    @Autowired
    CompanyService companyService;

    @GetMapping
    public List<Company> getAllCompany() {
        return companyService.findAll();
    }

    @GetMapping("{id}")
    public Company getById(HttpServletResponse response, @PathVariable("id") Long id) {
        return companyService.findById(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public String insert(@RequestBody Company company) {
        // TODO to return url to entity.
        companyService.insert(company);
        return "ok";
    }

    @PutMapping("{id}")
    public String update(@RequestBody Company company) {
         // TODO to return url to entity.
        companyService.update(company);
        return "ok";
    }

    @DeleteMapping("{id}")
    public String delete(@PathVariable("id") Long id) {
        companyService.delete(id);
        return "ok";
    }

}