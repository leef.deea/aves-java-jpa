package com.deea.aves.team;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
@Table(name="team")
public class Team {
   
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;
    @Column(name="parent_team_id")
    private Long parentTeamId;

    @Column(name="is_deleted")
    private boolean isDeleted;



    @Override
    public String toString() {
        return String.format(
                "Team[id=%d, name='%s', parent_team=%d, deleted=%s]",
                id, name, parentTeamId, isDeleted);
    }
}