package com.deea.aves.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletResponse;


@RestController
@RequestMapping("/teams")
@ResponseStatus(HttpStatus.OK)
public class TeamController {

    @Autowired
    TeamService teamService;

    @Autowired
    protected HttpServletResponse response;

    @GetMapping()
    public ResponseEntity<List<Team>> geAllTeams() {
        return new ResponseEntity<List<Team>>(teamService.findAll(), HttpStatus.OK);  
    }

    @GetMapping("{id}/childteams")
    public ResponseEntity<List<Team>> getChildTeam(@PathVariable("id") Long id) {

        return new ResponseEntity<List<Team>>(teamService.getByParentTeamId(id), HttpStatus.OK);

    }

    @GetMapping("{id}")
    public Optional<Team> getById(@PathVariable("id") Long id) {

        Optional<Team> ot = teamService.findById(id);
        if ( ot != null ) {
            return ot;
        } {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping()
    public String insert(@RequestBody Team team) {
        // TODO to return url to entity.
        teamService.insert(team);
        return "ok";
    }

  
    @PutMapping("{id}")
    public String update(@RequestBody Team team) {
        // TODO to return url to entity.
        teamService.update(team);
        return "ok";
    }


    @DeleteMapping("{id}")
    public String delete(@PathVariable("id") Long id) {
        teamService.deleteTeam(id);
        return "ok";
    }


}