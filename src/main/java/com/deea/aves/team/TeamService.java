package com.deea.aves.team;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeamService {

    @Autowired
    TeamRepository teamRepository;

    public List<Team> findAll() {
        return teamRepository.findAll();
    }

    public Optional<Team> findById( Long id ) {
        return teamRepository.findById(id);
    }

    public Long insert( Team team ) {
        return teamRepository.save(team).getId();
    }

    public List<Team> getByParentTeamId(Long id) {
        return teamRepository.findByParentTeamId(id);
    }


    public void update( Team team ) {
        teamRepository.save(team);
    }


    public String deleteTeam( Long id ) {
        
        Optional<Team> team = teamRepository.findById(id);
        if ( team.isPresent() ) {
            Team c = team.get();
            c.setDeleted(true);
            teamRepository.save(c);
            return "ok";
        }
        return "not found";
    }

      
}